const tabTitles = document.querySelectorAll('.menu-list');
const tabContent = document.querySelectorAll('.tabs-content');

function hideAllContentExcept(index) {
    for (let i = 0; i < tabContent.length; i++) {
        if (i !== index) {
            tabContent[i].style.display = 'none';
        } else {
            tabContent[i].style.display = 'flex';
        }
    }
}

hideAllContentExcept(0);

tabTitles.forEach((title, index) => {
    title.addEventListener('click', () => {
        hideAllContentExcept(index);
        tabTitles.forEach(t => t.classList.remove('active'));
        title.classList.add('active');
    });
});




$(document).ready(function() {
    let currentCategory = "all";

    $(".menu-amazing li").click(function () {
        let category = $(this).text().toLowerCase();
        currentCategory = category;
        showImagesByCategory(category);
        $(".menu-amazing li").removeClass("active");
        $(this).addClass("active");
    });

    function showImagesByCategory(category) {
        if (category === "all") {
            $(".trust-gallery-item").show();
        } else {
            $(".trust-gallery-item").hide();
            $(".trust-gallery-item." + category).show();
        }
    }




    function addImagesToGallery() {
        const images = [
            { path: "../img/amazing/amazing1.jpg", category: "graphicdesign" },
            { path: "../img/amazing/amazing2.jpg", category: "graphicdesign" },
            { path: "../img/amazing/amazing3.jpg", category: "graphicdesign" },
            { path: "../img/amazing/amazing4.jpg", category: "graphicdesign wordpress" },
            { path: "../img/amazing/amazing5.jpg", category: "landing" },
            { path: "../img/amazing/amazing6.jpg", category: "landing" },
            { path: "../img/amazing/amazing7.jpg", category: "landing wordpress" },
            { path: "../img/amazing/amazing8.jpg", category: "landing wordpress" },
            { path: "../img/amazing/amazing9.jpg", category: "webdesign" },
            { path: "../img/amazing/amazing10.jpg", category: "webdesign" },
            { path: "../img/amazing/amazing11.jpg", category: "webdesign" },
            { path: "../img/amazing/amazing12.jpg", category: "webdesign wordpress" },
        ];

        const newImagesSection = document.getElementById("new-images");
        let newImagesHtml = "";

        for (let i = 0; i < images.length; i++) {
            newImagesHtml += `
            <div class="image trust-gallery-item ${images[i].category}">
                <img src="${images[i].path}" style="width: 285px; height: 211px">
            </div>
        `;
        }

        newImagesSection.innerHTML += newImagesHtml;

        newImagesSection.style.paddingBottom = "100px";

        showImagesByCategory(currentCategory);

        const imagesDivs = newImagesSection.querySelectorAll(".image");

        imagesDivs.forEach(function (imageDiv) {
            const image = imageDiv.querySelector("img");

            image.addEventListener("mouseenter", function () {
                image.style.display = "none";

                const testImageDiv = document.createElement("a");
                testImageDiv.classList.add("test-image", "hoverable");
                testImageDiv.setAttribute("href", "#")
                testImageDiv.innerHTML = `
                     <svg class="icon-gallery">
                        <use xlink:href="#icon"></use>
                    </svg>
                    <h3 class="box-title">CREATIVE DESIGN</h3>
                    <p class="box-discription">Web Design</p>                
            `;
                testImageDiv.style.maxWidth = "285px"
                testImageDiv.style.maxHeight = "211px"

                imageDiv.appendChild(testImageDiv);
            });

            imageDiv.addEventListener("mouseleave", function () {
                const testImageDiv = imageDiv.querySelector(".test-image");
                if (testImageDiv) {
                    testImageDiv.remove();
                }
                image.style.display = "block";
            });
        });
    }

    let loadMoreBtn = document.getElementById("load-more-btn");
    loadMoreBtn.addEventListener("click", function () {
        addImagesToGallery();
        loadMoreBtn.style.display = "none";
    });
});



const dateBoxes = document.querySelectorAll('.date-box');

const currentDate = new Date();

const formattedDate = `${currentDate.getDate()}<br>${currentDate.toLocaleString('default', { month: 'short' }).toUpperCase()}`;

for (let i = 0; i < dateBoxes.length; i++) {
    dateBoxes[i].innerHTML = formattedDate;
}

for (let i = 0; i < dateBoxes.length; i++) {
    dateBoxes[i].style.display = 'flex';
    dateBoxes[i].style.justifyContent = 'center';
}




const sliderTitles = document.querySelectorAll('.slider-menu-list');
const sliderContent = document.querySelectorAll('.slider-content');
const sliderBack = document.querySelector('.slider-back');
const sliderNext = document.querySelector('.slider-next');
let activeIndex = 0;

function hideAllContentExceptSlider(index) {
    for (let i = 0; i < sliderContent.length; i++) {
        if (i !== index) {
            sliderContent[i].style.display = 'none';
        } else {
            sliderContent[i].style.display = 'flex';
        }
    }
}

hideAllContentExceptSlider(activeIndex);
sliderTitles[activeIndex].classList.add('active');

sliderTitles.forEach((title, index) => {
    title.addEventListener('click', () => {
        activeIndex = index;
        hideAllContentExceptSlider(activeIndex);
        sliderTitles.forEach(t => t.classList.remove('active'));
        title.classList.add('active');
    });
});

sliderBack.addEventListener('click', () => {
    activeIndex = (activeIndex - 1 + sliderTitles.length) % sliderTitles.length;
    hideAllContentExceptSlider(activeIndex);
    sliderTitles.forEach(t => t.classList.remove('active'));
    sliderTitles[activeIndex].classList.add('active');
});

sliderNext.addEventListener('click', () => {
    activeIndex = (activeIndex + 1) % sliderTitles.length;
    hideAllContentExceptSlider(activeIndex);
    sliderTitles.forEach(t => t.classList.remove('active'));
    sliderTitles[activeIndex].classList.add('active');
});
